class Fixnum
  def in_words
    return "zero" if self == 0
    self.thousand_up
  end

  def thousand_up
    str = ""
    num = self
    BIG_NUMS.each do |divisor, word|
      if num/divisor > 0
        str += (num/divisor).hundred_to_one + " " + word
        num = num % divisor
        str += " " if num > 0
      end
    end
    str += num.hundred_to_one if num > 0
    str
  end

  def hundred_to_one
    str = ""
    if self >= 100
      str += SMALL_NUMS[self/100] + " " + "hundred"
      str += " " if self%100 > 0
    end
    num = self % 100
    SMALL_NUMS.each do |divisor, word|
      if num / divisor > 0
        num %= divisor
        str += word
        str += " " if num > 0
      end
    end
    str
  end

  BIG_NUMS = { 1_000_000_000_000 => "trillion",
    1_000_000_000 => "billion",
    1_000_000 => "million",
    1_000 => "thousand"}

  SMALL_NUMS = { 100 => "hundred",
    90 => "ninety",
    80 => "eighty",
    70 => "seventy",
    60 => "sixty",
    50 => "fifty",
    40 => "forty",
    30 => "thirty",
    20 => "twenty",
    19 => "nineteen",
    18 => "eighteen",
    17 => "seventeen",
    16 => "sixteen",
    15 => "fifteen",
    14 => "fourteen",
    13 => "thirteen",
    12 => "twelve",
    11 => "eleven",
    10 => "ten",
    9 => "nine",
    8 => "eight",
    7 => "seven",
    6 => "six",
    5 => "five",
    4 => "four",
    3 => "three",
    2 => "two",
    1 => "one"}

end
